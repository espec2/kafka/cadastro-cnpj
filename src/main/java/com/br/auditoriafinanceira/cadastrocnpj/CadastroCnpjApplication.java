package com.br.auditoriafinanceira.cadastrocnpj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CadastroCnpjApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadastroCnpjApplication.class, args);
	}

}
