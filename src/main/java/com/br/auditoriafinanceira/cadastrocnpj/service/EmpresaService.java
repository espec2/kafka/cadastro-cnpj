package com.br.auditoriafinanceira.cadastrocnpj.service;

import com.br.auditoriafinanceira.cadastrocnpj.model.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService {

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    public void enviarAoKafka(Empresa empresa) {
        producer.send("spec2-andre-vinicius-2", empresa);
    }
}
