package com.br.auditoriafinanceira.cadastrocnpj.controller;

import com.br.auditoriafinanceira.cadastrocnpj.model.Empresa;
import com.br.auditoriafinanceira.cadastrocnpj.model.dto.CreateEmpresaRequest;
import com.br.auditoriafinanceira.cadastrocnpj.model.mapper.EmpresaMapper;
import com.br.auditoriafinanceira.cadastrocnpj.service.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/valida-empresa")
public class EmpresaController {
    @Autowired
    private EmpresaService empresaService;

    @PostMapping
    public void create(@RequestBody CreateEmpresaRequest createEmpresaRequest) {
        Empresa empresa = EmpresaMapper.fromCreateRequest(createEmpresaRequest);
        empresaService.enviarAoKafka(empresa);
    }
}
