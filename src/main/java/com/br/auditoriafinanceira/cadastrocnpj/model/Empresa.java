package com.br.auditoriafinanceira.cadastrocnpj.model;

import com.br.auditoriafinanceira.cadastrocnpj.enums.StatusCadastro;

public class Empresa {
    private String nome;
    private String cnpj;
    private StatusCadastro statusCadastro;

    public Empresa() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public StatusCadastro getStatusCadastro() {
        return statusCadastro;
    }

    public void setStatusCadastro(StatusCadastro statusCadastro) {
        this.statusCadastro = statusCadastro;
    }
}
