package com.br.auditoriafinanceira.cadastrocnpj.model.dto;

public class CreateEmpresaRequest {
    private String nome;
    private String cnpj;

    public CreateEmpresaRequest() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
}
