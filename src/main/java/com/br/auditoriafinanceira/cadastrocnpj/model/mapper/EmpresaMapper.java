package com.br.auditoriafinanceira.cadastrocnpj.model.mapper;

import com.br.auditoriafinanceira.cadastrocnpj.enums.StatusCadastro;
import com.br.auditoriafinanceira.cadastrocnpj.model.Empresa;
import com.br.auditoriafinanceira.cadastrocnpj.model.dto.CreateEmpresaRequest;

public class EmpresaMapper {

    public static Empresa fromCreateRequest(CreateEmpresaRequest empresaRequest) {
        Empresa empresa = new Empresa();

        empresa.setCnpj(empresaRequest.getCnpj());
        empresa.setNome(empresaRequest.getNome());
        empresa.setStatusCadastro(StatusCadastro.Invalido);

        return empresa;
    }
}
