package com.br.auditoriafinanceira.cadastrocnpj.enums;

public enum StatusCadastro {
        Valido,
        Invalido
    }
